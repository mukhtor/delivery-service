from django.urls import path, re_path
from . import views

app_name = "warehouse"

urlpatterns = (
    path('orders/', views.OrderListView.as_view(), name="orders"),
)
