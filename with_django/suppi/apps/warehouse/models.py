from django.db import models
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver


class MeasurementType(models.IntegerChoices):
    PIECES = 1
    KG = 2
    PACK = 3


class Tariff(models.Model):
    name = models.CharField(max_length=120)
    measurement = models.IntegerField(choices=MeasurementType.choices, default=MeasurementType.PIECES)
    cost = models.SmallIntegerField(default=0)

    def __str__(self):
        return f"{self.name} / ${self.cost}"


class Warehouse(models.Model):
    name = models.CharField(max_length=120)
    tariff = models.ForeignKey(Tariff, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.name} ({self.tariff.name})"


class Product(models.Model):
    name = models.CharField(max_length=120)
    price = models.SmallIntegerField(default=0)

    def __str__(self):
        return f"Product: {self.name} ({self.price})"


class Order(models.Model):
    order_id = models.CharField(max_length=120)
    warehouse = models.ForeignKey(Warehouse, on_delete=models.CASCADE)
    highway_cost = models.CharField(max_length=120, default="0")
    total = models.SmallIntegerField(default=0)
    products = models.ManyToManyField(Product, blank=True, through='OrderItem', related_name='products')

    def __str__(self):
        return f"Order: {self.order_id} ({self.warehouse.name})"

    def save(self, *args, **kwargs):
        if self.id is None:
            self.highway_cost = self.warehouse.tariff.cost
            # self.total = sum([x.price for x in self.products.all()])
        super(Order, self).save(*args, **kwargs)


class OrderItem(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name='order_item')
    quantity = models.PositiveSmallIntegerField(default=1)
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='product_items')

    def __str__(self):
        return f"Order item: {self.product.name} ({self.quantity})"


@receiver(post_save, sender=OrderItem)
def post_save_order(sender, instance: OrderItem, created, *args, **kwargs):
    if created:
        instance.order.total += instance.product.price
        instance.order.save()
