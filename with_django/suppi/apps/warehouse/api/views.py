from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.generics import ListAPIView
from .serializers import OrderListSerializers, Order


class OrderListView(ListAPIView):
    """
    GET: http://127.0.0.1:3131/api/orders/
    """
    permission_classes = (AllowAny,)
    serializer_class = OrderListSerializers
    queryset = Order.objects.all()

