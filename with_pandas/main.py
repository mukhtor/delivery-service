import pandas as pd

# Заданные данные
data = [
    {
        "order_id": 11973,
        "warehouse_name": "Мордор",
        "highway_cost": -70,
        "products": [
            {
                "product": "ломтик июльского неба",
                "price": 450,
                "quantity": 1
            },
            {
                "product": "билет в Израиль",
                "price": 1000,
                "quantity": 3
            },
            {
                "product": "статуэтка Ленина",
                "price": 200,
                "quantity": 3
            }
        ]
    },
    {
        "order_id": 62239,
        "warehouse_name": "хутор близ Диканьки",
        "highway_cost": -15,
        "products": [
            {
                "product": "билет в Израиль",
                "price": 1000,
                "quantity": 1
            }
        ]
    },
    {
        "order_id": 85794,
        "warehouse_name": "отель Лето",
        "highway_cost": -50,
        "products": [
            {
                "product": "зеленая пластинка",
                "price": 10,
                "quantity": 2
            }
        ]
    },
    {
        "order_id": 33684,
        "warehouse_name": "Мордор",
        "highway_cost": -30,
        "products": [
            {
                "product": "билет в Израиль",
                "price": 1000,
                "quantity": 2
            },
            {
                "product": "зеленая пластинка",
                "price": 10,
                "quantity": 1
            }
        ]
    },
    {
        "order_id": 25824,
        "warehouse_name": "отель Лето",
        "highway_cost": -75,
        "products": [
            {
                "product": "автограф Стаса Барецкого",
                "price": 600,
                "quantity": 1
            },
            {
                "product": "статуэтка Ленина",
                "price": 200,
                "quantity": 1
            },
            {
                "product": "плюмбус",
                "price": 250,
                "quantity": 1
            }
        ]
    },
    {
        "order_id": 87044,
        "warehouse_name": "остров невезения",
        "highway_cost": -15,
        "products": [
            {
                "product": "плюмбус",
                "price": 250,
                "quantity": 3
            }
        ]
    },
    {
        "order_id": 58598,
        "warehouse_name": "гиперборея",
        "highway_cost": -160,
        "products": [
            {
                "product": "плюмбус",
                "price": 250,
                "quantity": 3
            },
            {
                "product": "статуэтка Ленина",
                "price": 200,
                "quantity": 3
            },
            {
                "product": "статуэтка Ленина",
                "price": 200,
                "quantity": 2
            }
        ]
    },
    {
        "order_id": 5430,
        "warehouse_name": "гиперборея",
        "highway_cost": -80,
        "products": [
            {
                "product": "ломтик июльского неба",
                "price": 450,
                "quantity": 1
            },
            {
                "product": "плюмбус",
                "price": 250,
                "quantity": 3
            }
        ]
    },
    {
        "order_id": 60502,
        "warehouse_name": "отель Лето",
        "highway_cost": -75,
        "products": [
            {
                "product": "автограф Стаса Барецкого",
                "price": 600,
                "quantity": 3
            }
        ]
    },
    {
        "order_id": 96473,
        "warehouse_name": "Мордор",
        "highway_cost": -20,
        "products": [
            {
                "product": "зеленая пластинка",
                "price": 10,
                "quantity": 2
            }
        ]
    },
    {
        "order_id": 64013,
        "warehouse_name": "хутор близ Диканьки",
        "highway_cost": -105,
        "products": [
            {
                "product": "плюмбус",
                "price": 250,
                "quantity": 2
            },
            {
                "product": "плюмбус",
                "price": 250,
                "quantity": 2
            },
            {
                "product": "зеленая пластинка",
                "price": 10,
                "quantity": 3
            }
        ]
    },
    {
        "order_id": 59105,
        "warehouse_name": "отель Лето",
        "highway_cost": -25,
        "products": [
            {
                "product": "статуэтка Ленина",
                "price": 200,
                "quantity": 1
            }
        ]
    },
    {
        "order_id": 85535,
        "warehouse_name": "отель Лето",
        "highway_cost": -25,
        "products": [
            {
                "product": "статуэтка Ленина",
                "price": 200,
                "quantity": 1
            }
        ]
    }
]


def calculate_inc_exp_prof():
    income = 0
    expenses = 0
    profit = 0
    for item in data:
        inc = 0
        exp = 0
        for product in item["products"]:
            inc += product["price"] * product["quantity"]
            exp += item["highway_cost"] * product["quantity"]
        item_profit = inc - exp
        income += inc
        expenses += exp
        profit += item_profit
    return income, expenses, profit


# Создаем DataFrame из данных
df = pd.DataFrame(data)

# Создать DataFrame с уникальными складами и их тарифами
warehouse_tariffs = df.groupby("warehouse_name")["highway_cost"].mean().reset_index()

# Вывести результат
print(warehouse_tariffs)
print("--------------------------------")
print()

# Развернуть список товаров в отдельные строки

# Развернуть список товаров в отдельные строки
df_products = df.explode("products")

# Создать отдельные столбцы для каждого атрибута продукта
df_products = pd.concat([df_products.drop(["products"], axis=1), df_products["products"].apply(pd.Series)], axis=1)

# Рассчитать доход, расход и прибыль для каждой строки
df_products["income"] = df_products["price"] * df_products["quantity"]
df_products["expenses"] = df_products["highway_cost"] * df_products["quantity"]
df_products["profit"] = df_products["income"] - df_products["expenses"]

# Группировка по товару и агрегирование суммарных значений
summary_by_product = df_products.groupby("product").agg(
    quantity=("quantity", "sum"),
    income=("income", "sum"),
    expenses=("expenses", "sum"),
    profit=("profit", "sum"),
).reset_index()

# Вывести результат
print(summary_by_product)
print("--------------------------------")
print()

# Задача 3: Составить табличку со столбцами 'order_id' и 'order_profit', а также вывести среднюю прибыль заказов
# Рассчитать прибыль для каждого заказа
df["order_profit"] = df_products.groupby(df_products.index)["profit"].sum()

# Создать таблицу с заказами и их прибылью
order_profit_table = df[["order_id", "order_profit"]]

# Рассчитать среднюю прибыль заказов
average_profit = order_profit_table["order_profit"].mean()

# Вывести результаты
print(order_profit_table)
print("Средняя прибыль заказов:", average_profit)

# Задача 4: Составить таблицу со столбцами 'warehouse_name', 'product', 'quantity', 'profit', 'percent_profit_product_of_warehouse' (процент прибыли продукта заказанного из определенного склада к прибыли этого склада)
# Развернуть список товаров в отдельные строки и добавить столбец с прибылью склада
df_products_with_warehouse_profit = df_products.merge(
    warehouse_tariffs, on="warehouse_name", how="left"
)
df_products_with_warehouse_profit["warehouse_profit"] = (
    df_products_with_warehouse_profit["products"]["price"]
    * df_products_with_warehouse_profit["products"]["quantity"]
    - df_products_with_warehouse_profit["highway_cost"]
    * df_products_with_warehouse_profit["products"]["quantity"]
)

# Рассчитать процент прибыли продукта от прибыли склада
df_products_with_warehouse_profit["percent_profit_product_of_warehouse"] = (
    df_products_with_warehouse_profit["profit"] / df_products_with_warehouse_profit["warehouse_profit"] * 100
)

# Создать таблицу с необходимыми столбцами
warehouse_product_profit_table = df_products_with_warehouse_profit[[
    "warehouse_name",
    "products.product",
    "products.quantity",
    "profit",
    "percent_profit_product_of_warehouse",
]]

# Вывести результат
print(warehouse_product_profit_table)
print("--------------------------------")
print()
# Задача 5: Отсортировать 'percent_profit_product_of_warehouse' по убыванию, после посчитать накопленный процент ('accumulated_percent_profit_product_of_warehouse')

# Отсортировать по убыванию
warehouse_product_profit_table_sorted = warehouse_product_profit_table.sort_values(
    by="percent_profit_product_of_warehouse", ascending=False
)

# Рассчитать накопленный процент
warehouse_product_profit_table_sorted["accumulated_percent_profit_product_of_warehouse"] = (
    warehouse_product_profit_table_sorted["percent_profit_product_of_warehouse"].cumsum()
)

# Вывести результат
print(warehouse_product_profit_table_sorted)
print("--------------------------------")
print()

# Задача 6: Присвоить A, B, C - категории на основании значения накопленного процента ('accumulated_percent_profit_product_of_warehouse')

# Создать функцию для определения категории
def assign_category(percentage):
    if percentage <= 70:
        return "A"
    elif percentage <= 90:
        return "B"
    else:
        return "C"

# Применить функцию к столбцу 'accumulated_percent_profit_product_of_warehouse'
warehouse_product_profit_table_sorted["category"] = warehouse_product_profit_table_sorted["accumulated_percent_profit_product_of_warehouse"].apply(assign_category)

# Вывести результат
print(warehouse_product_profit_table_sorted)

