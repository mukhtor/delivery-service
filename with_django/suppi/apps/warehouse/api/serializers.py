from rest_framework import serializers
from ..models import Order, OrderItem, Product


class ProductSerializers(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('name', 'price')


class OrderItemSerializers(serializers.ModelSerializer):
    product = serializers.CharField(source='product.name')
    price = serializers.IntegerField(source='product.price')

    class Meta:
        model = OrderItem
        fields = ('product', 'price', 'quantity')


class OrderListSerializers(serializers.ModelSerializer):
    warehouse_name = serializers.CharField(source='warehouse.name')
    highway_cost = serializers.IntegerField(read_only=True)
    products = OrderItemSerializers(source='order_item', many=True)

    class Meta:
        model = Order
        fields = (
            'order_id',
            'warehouse_name',
            'highway_cost',
            'total',
            'products',
        )
