from django import forms
from django.contrib import admin
from django.contrib.auth.models import Group
from apps.warehouse import models
admin.site.unregister(Group)


admin.site.register(models.Tariff)
admin.site.register(models.Warehouse)
admin.site.register(models.Product)
admin.site.register(models.Order)
admin.site.register(models.OrderItem)

